<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Niko extends Model
{
    protected $table = 'topicos';
    protected $fillable = [
        'nombre',
        'apellidoP',
        'apellidoM',
        'correo_electronico',
        'edad',
        'edad2',
    ];
    protected $attributes = [
        'nombre' => '',
        'apellidoP' => '',
        'apellidoM' => '',
        'correo_electronico' => '',
        'edad' => '',
        'edad2'=> '',
    ];
}
